console.log("Hello! Plank started up.");

var fs = require("fs");
// Datastore
var Datastore = require('nedb')
var people = new Datastore({ filename: './datastores/people.json', autoload: true });
var logger = new Datastore({ filename: './datastores/people_log.json', autoload: true });

// Slack
var token = fs.readFileSync("./configs/slack_token.txt").toString();
var Slack = require('slack-client');
var slack = new Slack(token, true, true);

// Pusher
var Pusher = require('pusher-client');
var pusher_data = JSON.parse(fs.readFileSync("./configs/pusher_config.json").toString());
var pusher = new Pusher(pusher_data.key, {encrypted: true});

// Submodules
// var tracker = require('./plank.people');
var menu = require('./plank.menu');
menu.setSlack(slack);
// var door = require('./plank.door');
// door.setSlack(slack);

// // Pusher Listener
var channel_door = pusher.subscribe('door');
channel_door.bind('inside', function(data) {
  console.log("PUSHER: Inside",data);
  // console.log(door.openInsideDoor());
});
channel_door.bind('outside', function(data) {
  console.log("PUSHER: Outside",data);
  // console.log(door.openOutsideDoor());
});
channel_door.bind('both', function(data) {
  console.log("PUSHER: Both",data);
  // console.log(door.openBothDoors());
});
channel_door.bind('reset', function(data) {
  console.log("PUSHER: Reset",data);
  // console.log(door.resetStates());
});


// Slack Listener
slack.on('message', function(message) {
  var channel = slack.getChannelGroupOrDMByID(message.channel);
  var user = slack.getUserByID(message.user);
  // Make sure the message is a private message.
  if(message.type === 'message' && user !== undefined)
  {
    if(slack.self.name !== user.name && user.name === channel.name)
    {
      menu.message(channel, user.name, message.text);
      // door.message(channel, user.name, message.text);
      processSlackMessage(channel, message.text);
    }
  }
});

slack.login();


// TODO extract plank.people stuff to its own file

function processSlackMessage(channel, _message) {
  _message = _message.toLowerCase();
  if(_message == "hey" || _message == "hi" || _message == "hello" || _message == "hallo" || _message == "yo" || _message == "sup" || _message == "ola")
  {
    channel.send("Hello!");
    return true;
  }

  // People tracking // TODO abstract
  else if(_message === "people")
  {
    people.find({}, function (err, docs) {
      var content = "Here's a list of all the tracked people:\n";
      docs.forEach(function(d){
        var status = d.status == "here" ? "is at the office since" : "left the office at";
        var date = d.last_seen == "never" ? "never" : d.last_seen.toTimeString().split(' ')[0];
        var c = "\n• *"+d.username+"* " + status + " " + date + ".\n";
        console.log(c);
        content += c;
      });
      channel.send(content + "");
    });
    return true;
  }
  else if(_message === "here" || _message === "there")
  {
    var headcount = 0;
    people.find({status: "here"}, function (err, docs) {
      var content = "";
      docs.forEach(function(d){
        var status = d.status == "here" ? "is at the office since" : "left the office at";
        var date = d.last_seen == "never" ? "never" : d.last_seen.toTimeString().split(' ')[0];
        var c = "\n• *"+d.username+"* " + status + " " + date + ".";
        console.log(c);
        content += c;
        headcount++;
      });
      if(headcount > 0)
        channel.send("These people are at the office now:" + content + "");
      else
        channel.send("My home is a ghost town! There's no one here with me. :cold_sweat:");
    });
    return true;
  }
  else if(_message.substring(0, 10) === "add person")
  {
    var stuff = _message.split("add person ").pop();
    var username = stuff.split(" ")[0];
    var mac = stuff.split(" ")[1];
    people.insert({username: username, mac: mac, last_seen: "never", status: "away"});
    logger.insert({action: "created", created_at: new Date(), person: username});
    channel.send("Thanks for adding "+username+" to the system.\nHe's office activity will now be tracked.");
    channel.send("Go get some coffee, you deserve it!");
    return true;
  }
  else if(_message.substring(0, 13) === "remove person")
  {
    var username = _message.split("remove person ").pop();
    people.remove({ username: username }, {});
    logger.insert({action: "removed", created_at: new Date(), person: username});
    channel.send("Person removed.");
    channel.send("Eish. :grimacing:");
    return true;
  }
  else if(_message === "help")
  {
    var content = "Stuck? Meh! I'm not that clever, but I can respond to the following:\n"
        +"*Menu:*\n"
        +">`menu` - Shows the available lunch menu.\n"
        +">`menu orders` - Shows what is to be ordered today.\n"
        +">`menu add [name]` - Adds default item to the current order.\n"
        +">`menu add [name] [item number OR custom text]` - Adds item to the current order.\n"
        +">`menu remove [order item number]` - Removes item from list.\n"
        +">`menu week` - Show what default meals are planned for the week.\n"
        +">`menu set [1..5] [item number OR custom text]` - Set the default for this day of the week.\n"
        +">`menu help` - Show all the available menu options and instructions.\n"
        +"*Door:*\n"
        +">`door` - Temporarily unlock inside office door.\n"
        +">`door outside` - Allows the first bell ring to office open lobby door.\n"
        +">`door both` - Allows the first bell ring to office open both lobby and office door.\n"
        +">`door help` - Shows all available door commands.\n"
        +"*People:*\n"
        +">`open` - opens the front door (not really).\n"
        +">`people` - lists all the people on the system.\n"
        +">`here` _or_ `there` - lists everyone that's here in the office with me.\n"
        +">`add person [username] [mac]` - add a person to the system.\n"
        +">`remove person [username]` - remove a person from the system.\n"
        +">`help` - will get you right back where you are now.\n"
    channel.send(content);
    return true;
  }
  else
  {
    channel.send("Uhm... what!? Type `help` for some tips on how you can speak _woodwork_.");
    return true;
  }
  return false;
}

// //tracker.start(slack, people, logger);
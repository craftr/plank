# Plank
Hi, my name is Plank. I'm a [Slack](https://slack.com/)-bot developed in Node.js and designed to be hosted on a [Raspberry Pi](https://www.raspberrypi.org/). I'm also tightly integrated to an iPhone app and some cloud services. Feel free to use and abuse me. I would appreciate any forks and updates.

## Current features include:
 - Track/notify when team members enter or leave office *(almost done)*
 - Open office door *(almost done)*

## Slack Interface

|Command|Function|
|---|---|
|`/door`|Temporarily unlock office door.|
|`/door outside`|Allows the first bell ring to office open lobby door.|
|`/door both`|Allows the first bell ring to office open both lobby and office door.|
|`/here`|Shows who's at the office.|
|`/person add [username] [macaddress]`|Add user to track presence.|
|`/person remove [username]`|Remove user from being tracked.|
|`/person allow [username]`|Allow user to mangage bot.|
|`/person ban [username]`|Un-allow user to manage bot.|
|`/help`|Shows all available commands.|

*Anything that is not a valid command will get a random response from a long list of “comments” from the bot.*

## Email Integration
A NodeJS MailChimp package will allow Plank to send emails to the outside world.

## Pusher Integration
Pusher integration for Plank will allow both the iOS app and the links sent in emails to reach Plank in realtime. This will only be needed if the static IP can't be directed to Plank's NodeJS server.

## Future Ideas & Features
 - Play Coffitivity or white/brown noise type of background sound.
 - Todo list for channels and personal
 - Lunch menu & ordering
 - Door open invite

## Idea Definition & Design

### Door Open Invite
An interface where a link is emailed to a friend/person that allows him to open the door once by opening the link mailed to him.

|Command|Function|
|---|---|
|`/door invite [email address]`|Sends a one-time link to the email that will perform `/doors` command.|

### Lunch Menu & Ordering

This feature will allow Plank to automatically order lunch for everyone every day. It will currently integrate with Smulpaap's menu. (Single foodz provider)

A fixed office menu selection will be added which will rotate weekly. This will allow users to simply select "food please!" if they're not in the mood to select something unique.

**Ordering Process:**
1) Everyone has till 9:00am to confirm their order. (They can change it if they want from the standard)
2) At 9:00am Plank will post the full order on the `#office` channel for someone to confirm or change if needed.
3) At 9:30am Plank will send the order via email to the foodz provider.

|Command|Function|
|---|---|
|`/menu`|Shows the available lunch menu.|
|`/menu orders`|Shows what is to be ordered today.|
|`/menu add [username]`|Adds default item to the current order.|
|`/menu add [username] [item-number OR text]`|Adds another item to the current order.|
|`/menu remove [order-item-number]`|Removes item from list.|
|`/menu week`|Show what default meals are planned for the week.|
|`/menu set [day] [item-number OR text]`|Set the default for this day of the week.|
|`/menu help`|Show all the available menu options and instructions.|


## Contributors
 - [Louw Hopley](https://bitbucket.org/LouwHopley)

## Modules used / Credit
 - [arpjs](https://github.com/skepticfx/arpjs)
 - [ping](http://github.com/danielzzz/node-ping)
 - [nedb](https://github.com/louischatriot/nedb)
 - [node-slack-client](https://github.com/slackhq/node-slack-client)
var arp, exec, logger, people, ping, slack, sys;

// Update all DB records to their current ARP status
function processARP(_arp_table) {
  people.update({}, {$set: {arp_status: "away", arp_ip: ""}}, {}, function (err, numReplaced) {
    console.log("[Tracker]: processARP: 1. Make all 'away'");
    _arp_table.forEach(function(row) {
      var mac = row["mac"];
      var ip = row["ip"];
      people.update({mac: mac}, {$set: {arp_status: "here", arp_ip: ip}}, {}, function (err, numReplaced) {
        console.log("[Tracker]: processARP: 2. Updated",mac,"to 'here'.");
      });
    });
  });
}

// Figure out who left or arrived
function processChanges() {
  people.find({}, function (err, docs) {
    docs.forEach(function(d) {
      if(d.status === "here" && d.arp_status === "away")
        checkingOut(d);
      else if(d.status === "away" && d.arp_status === "here")
        checkingIn(d);
    });
  });
}

// Mark a person as checked in. Post on Slack.
function checkingIn(_person) {
  console.log("Arrived:", _person.username);
  people.update({mac: _person.mac}, {$set: {last_seen: new Date(), status: "here"}}, {});
  logger.insert({action: "arrived", created_at: new Date(), person: _person.username});
}

// Mark a person as checked out. Post on Slack.
function checkingOut(_person) {
  console.log("Departed:", _person.username);
  people.update({mac: _person.mac}, {$set: {last_seen: new Date(), status: "away"}}, {});
  logger.insert({action: "departed", created_at: new Date(), person: _person.username});
}

module.exports = {
  start: function (_slack, _people, _logger) {
    arp = require('arpjs');
    ping = require ("ping");
    sys = require('sys');
    exec = require('child_process').exec;
    slack = _slack;
    people = _people;
    logger = _logger;

    // Update the DB with ARP statusses
    setInterval(function(){
      arp.table(function(err, table){
        processARP(table);
      });
    }, 300);

    // Interval checker for changes
    setInterval(function(){
      processChanges();
    }, 1000);

    // ARP caching takes a few minutes to refresh,
    // to counter this issue. Let's confirm presence
    // with a ping and update the cache for that host
    // if needed.
    setInterval(function(){
      people.find({arp_status: "here"}, function (err, docs) {
        docs.forEach(function(d) {
          var host = d.ip;
          ping.sys.probe(host, function(isAlive) {
            if(!isAlive)
            {
              // Refresh ARP
              exec("arp -d " + host, function (error, stdout, stderr) { });
            }
          });
        });
      });
    }, 1000);
  }
};
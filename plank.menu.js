var slack;
var fs = require("fs");

// Configure datastore
var Datastore = require('nedb')
var menu_orders = new Datastore({ filename: './datastores/menu_orders.json', autoload: true });
var menu = JSON.parse(fs.readFileSync("./configs/smulpaap_menu.json").toString());

// Configure Nodemailer
var nodemailer = require('nodemailer');
var fs = require("fs");
var config_data = JSON.parse(fs.readFileSync("./configs/mail_config_menu.json").toString());
var transporter = nodemailer.createTransport(config_data.transport);
var mailOptions = config_data.config;

// To send a message:
// mailOptions.config.subject = "Food Order dd/mm/yyyy";
// mailOptions.config.text = "...";
// mailOptions.config.html = "...";
// transporter.sendMail(mailOptions, function(error, info){
//   if(error){
//     return console.log(error);
//   }
//   console.log('Message sent: ' + info.response);
// });

/*

TODO:
1) Email feature (check)
2) Scheduled jobs
3) Next job date
4) Help list

*/

module.exports = {
  setSlack: function (_slack) {
    slack = _slack;
  },
  message: function (channel, username, message) {
    // Filter out non-menu messages
    if(message.substring(0,4).toLowerCase() !== "menu")
      return;

    // Show available menu
    if(message.toLowerCase() === "menu")
    {
      channel.send(getMenu());
      return;
    }

    // Show all the orders for the day
    if(message.toLowerCase() === "menu orders")
    {
      getOrders(channel);
      return;
    }

    // Add an item to the latest order
    if(message.toLowerCase().substring(0, 8) === "menu add")
    {
      var params = message.substring(9);
      channel.send(addOrderItem(params));
      return;
    }

    // Remove an item from the current order
    if(message.toLowerCase().substring(0, 11) === "menu remove")
    {
      var item = message.substring(12);
      channel.send(removeOrderItem(item));
      return;
    }

    // Show the default weekly menu
    if(message.toLowerCase() === "menu week")
    {
      channel.send(getWeek());
      return;
    }

    // Remove an item from the current order
    if(message.toLowerCase().substring(0, 8) === "menu set")
    {
      var params = message.substring(9);
      channel.send(setDefaultItem(params));
      return;
    }

    // Show help
    if(message.toLowerCase() === "menu help")
    {
      channel.send(getHelp());
      return;
    }

  }
};

function getMenu() {
  var m = "";
  var count = 1;
  // Compile menu from JSON
  for (var i in menu.sections)
  {
    var section = menu.sections[i];
    m += ">*"+section.name+"*\n";
    for (var j in section.items)
    {
      var item = section.items[j];
      m += ">"+count +". _"+item+"_\n";
      count++;
    }
  }
  return "Here is the available menu from Smulpaap:\n\n" + m + "You can view the complete menu here: http://bit.ly/1Pwl0IG";
}

function getOrders(channel) {
  var m = "Here is the current list of orders:\n";

  var d = getNextOrderDate();
  menu_orders.find({ order_date: d.short }, function (err, docs) {
    for (var i in docs)
    {
      var order = docs[i];
      var c = parseInt(i + 1);
      m += ">" + c + ". "+order.user+": _"+order.order+"_\n";
    }
    channel.send(m);
  });
  return m;
}

function addOrderItem(params)
{
  var result;
  var username;
  var order;

  var d = getNextOrderDate();

  // Decode instructions
  if(params.indexOf(" ") === -1) // Only username provided
  {
    username = params;
    order = menu.defaults[d.day];
    result = "Adding default order, "+order+", for: " + username;
  }
  else
  {
    username = params.substring(0, params.indexOf(" "));
    order = params.substring(params.indexOf(" ") + 1);
    if(isInt(order))
    {
      result = "Adding menu item "+order+" for "+username;
      order = getMenuItem(order);
    }
    else
    {
      result = "Adding custom item '"+order+"' for "+username;
    }
  }

  // Create order
  menu_orders.insert({order_date: d.short, created_at: new Date(), order: order, user: username});

  return result;
}

function removeOrderItem(number) {
  number = number - 1;
  var d = getNextOrderDate();
  menu_orders.find({ order_date: d.short }, function (err, docs) {
    var item = docs[number];
    if(item !== undefined)
    {
      var _id = item._id;
      menu_orders.remove({ _id: _id }, {}, function (err, numRemoved) {
        console.log("MENU: Item removed from order.");
      });
    }
    else
    {
      return "Could not remove item. No such item exists.";
    }

  });
  var c = number + 1;
  return "Removing item from order: " + c;
}

function getWeek() {
  var m = "";
  for(var i in menu.defaults)
  {
    var o = menu.defaults[i];
    m += ">" + i + ": _"+o+"_\n";
  }
  return "Here is the default weekly meals:\n" + m;
}

function setDefaultItem(params) {
  var day = params.substring(0, params.indexOf(" "));
  var order = params.substring(params.indexOf(" ") + 1);
  if(isInt(order))
  {
    order = getMenuItem(order);
  }
  menu.defaults[day] = order;
  var json = JSON.stringify(menu, null, 2);
  fs.writeFile("./configs/smulpaap_menu.json", json, function(err) {
    console.log("Done.");
  });
  return "Setting default as '"+order+"' for day "+day;
}

function getHelp() {
  return "This is all the available features of the menu:\n"
        +">`menu` - Shows the available lunch menu.\n"
        +">`menu orders` - Shows what is to be ordered today.\n"
        +">`menu add [name]` - Adds default item to the current order.\n"
        +">`menu add [name] [item number OR custom text]` - Adds item to the current order.\n"
        +">`menu remove [order item number]` - Removes item from list.\n"
        +">`menu week` - Show what default meals are planned for the week.\n"
        +">`menu set [1..5] [item number OR custom text]` - Set the default for this day of the week.\n"
        +">`menu help` - Show all the available menu options and instructions.";
}

function getNextOrderDate() {
  var d = new Date(); // TODO get day of next order instead
  var o = {
    full: d,
    day: 1,//d.getDay(),
    short: d.toISOString().slice(0, 10)
  };
  return o;
}

function getMenuItem(number) {
  var count = 1;
  // Compile menu from JSON
  for (var i in menu.sections)
  {
    var section = menu.sections[i];
    for (var j in section.items)
    {
      var item = section.items[j];
      if(count == number)
        return item;
      count++;
    }
  }
}

function isInt(value) {
  return !isNaN(value) && parseInt(Number(value)) == value && !isNaN(parseInt(value, 10));
}
// Configure datastore
var Datastore = require('nedb')
var door_log = new Datastore({ filename: './datastores/door_log.json', autoload: true });

// Door states
var state_pending_outside = false;
var state_open_inside_after_outside = false;
var pin_inside = 16;
var pin_phone = 17;
var pin_outside = 18;

// GPIO
var gpio = require("rpi-gpio");


/*

TODO:
1) Trigger on call?

*/

function outsideTriggered() {
  if(state_pending_outside === false)
    return;
  state_pending_outside = false;

  // Setup pin for phone
  gpio.setup(pin_phone, gpio.DIR_OUT, function(){
    // Pick up phone
    gpio.write(pin_phone, true, function(err1) {
      if (err1) throw err1;
      console.log('DOOR: Picked up phone.');
      // Setup pin for bell button
      gpio.setup(pin_outside, gpio.DIR_OUT, function(){
        // Trigger bell button
        gpio.write(pin_outside, true, function(err2) {
          if (err2) throw err2;
          console.log('DOOR: Opened outside door, now waiting.');
          door_log.insert({action: "triggered outside", created_at: new Date()});

          // 'Hold button' for 1 second
          setTimeout(function(){
            gpio.write(pin_outside, false, function(err) {
              console.log('DOOR: Closed outside door.');
            });
            gpio.write(pin_phone, false, function(err) {
              console.log('DOOR: Phone put down.');
            });
          }, 1000);

          // Open inside door if requested
          openInsideDoor(20000);

        }); // Outside write
      }); // Outside setup
    }); // Phone write
  }); // Phone setup
}


function openInsideDoor(duration) {
  duration = typeof a !== 'undefined' ? duration : 5000;
  gpio.setup(pin_inside, gpio.DIR_OUT, function(){
    gpio.write(pin_inside, true, function(err) {
      if (err) throw err;
      console.log('DOOR: Opened inside door, now waiting.');
      door_log.insert({action: "triggered inside", created_at: new Date()});

      // Wait 5 seconds before closing
      setTimeout(function(){
        gpio.write(pin_inside, false, function(err) {
          console.log('DOOR: Closed inside door.');
        });
      }, duration);
    });
  });
  return "Inside door triggered.";
}

function openOutsideDoor() {
  state_pending_outside = true;
  state_open_inside_after_outside = false;
  return "Outside door will now open upon first bell ring.";
}

function openBothDoors() {
  state_pending_outside = true;
  state_open_inside_after_outside = true;
  door_log.insert({action: "both", created_at: new Date()});
  return "Outside & inside door will now open upon first bell ring.";
}
function resetStates() {
  state_pending_outside = false;
  state_open_inside_after_outside = false;
  return "Doors are reset to locked.";
}

function getHelp() {
  return "Here is a list of all available commands to manage the doors:\n"
        +">`door` - Temporarily unlock inside office door.\n"
        +">`door outside` - Allows the first bell ring to office open lobby door.\n"
        +">`door both` - Allows the first bell ring to office open both lobby and office door.\n"
        +">`door help` - Shows all available door commands.";
}

module.exports = {
  setSlack: function (_slack) {
    slack = _slack;
  },
  message: function (channel, username, message) {
    // Filter out non-door messages
    if(message.toLowerCase().substring(0,4) !== "door")
      return;

    // Open door
    if(message.toLowerCase() === "door")
    {
      door_log.insert({action: "slack inside", username: username, created_at: new Date()});
      channel.send(openInsideDoor());
      return;
    }

    // Open outsite door.
    if(message.toLowerCase() === "door outside")
    {
      door_log.insert({action: "slack outside", username: username, created_at: new Date()});
      channel.send(openOutsideDoor());
      return;
    }

    // Open outsite door.
    if(message.toLowerCase() === "door both")
    {
      door_log.insert({action: "slack both", username: username, created_at: new Date()});
      channel.send(openBothDoors());
      return;
    }

    // Show reset
    if(message.toLowerCase() === "door reset")
    {
      channel.send(resetStates());
      return;
    }

    // Show help
    if(message.toLowerCase() === "door help")
    {
      channel.send(getHelp());
      return;
    }

  },
  openInsideDoor: openInsideDoor,
  openOutsideDoor: openOutsideDoor,
  openBothDoors: openBothDoors,
  resetStates: resetStates
};
